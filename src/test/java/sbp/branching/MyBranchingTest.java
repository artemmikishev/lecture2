package sbp.branching;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import sbp.common.Utils;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;


/**
 * Класс с тестами для {@link MyBranching}
 */
public class MyBranchingTest
{
    /**
     * Проверка первого метода maxInt()
     */

    /**
     * В этом тесте, метод utilFunc2() - true, и метод utilFunc1(anyString) - true
     * при этих параметрах метод maxInt должен возвращать 0
     */
    @Test
    public void maxIntTest_Success_Success()
    {
        Utils utilsMock = Mockito.mock(Utils.class);

        MyBranching myBranching = new MyBranching(utilsMock);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(true);

        Assertions.assertEquals(0, myBranching.maxInt(1500, -2000));
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1(Mockito.anyString());


    }

    /**
     * В этом тесте, метод utilFunc2() - true, и метод utilFunc1(anyString) - false,
     * при этих параметрах метод maxInt должен возвращать 0.
     * Если первое число отрицательное, то функция utilFunc1(anyString) не будет вызываться, т.к. метод не зайдет в цикл.
     */
    @Test
    public void maxIntTest_Success_Negative()
    {
        Utils utilsMock = Mockito.mock(Utils.class);

        MyBranching myBranching = new MyBranching(utilsMock);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(false);

        myBranching.maxInt(-1500, 2000);
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsMock, Mockito.never()).utilFunc1(Mockito.anyString());



        Assertions.assertEquals(0, myBranching.maxInt(1500, -2000));






    }

    /**
     * В этом тесте, метод utilFunc2() - false, при таком параметре, мы вообще не должны "заходить" в циклы,
     * метод возвращает большее из чисел
     * проверяем
     */
    @Test
    public void maxIntTest()
    {
        Utils utilsMock = Mockito.mock(Utils.class);

        MyBranching myBranching = new MyBranching(utilsMock);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(false);


        Mockito.verify(utilsMock, Mockito.never()).utilFunc1(Mockito.anyString());
        Mockito.verify(utilsMock, Mockito.never()).utilFunc2();

        Assertions.assertEquals(1500, myBranching.maxInt(1500, -2000));
        Assertions.assertEquals(2000, myBranching.maxInt(-1500, 2000));
        Assertions.assertEquals(-1500, myBranching.maxInt(-1500, -2000));
    }



    /**
     * Проверка второго метод ifElseExample()
     * Проверка успешного сценария выполнения функции utilFunc2()
     */
    @Test
    public void ifElseExample_Success_Test()
    {
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsMock);
        boolean result = myBranching.ifElseExample();
        Assertions.assertTrue(result);

        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsMock, Mockito.never()).utilFunc1(Mockito.anyString());
    }

    /**
     * Проверка неуспешного сценария выполнения функции utilFunc2()
     */
    @Test
    public void ifElseExample_Negative_Test()
    {
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);

        MyBranching myBranching = new MyBranching(utilsMock);
        boolean result = myBranching.ifElseExample();
        Assertions.assertFalse(result);

        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsMock, Mockito.never()).utilFunc1(Mockito.anyString());

    }

    /**
     * Проверка второго метод switchExample()
     * Проверка сценария, когда, в функцию подаётся на вход, число i = 1
     * должен вызваться метод utilFunc1(anyString) и метод utilFunc2() т.к. break только во втором case
     */
    @Test
    public void switchExample_One()
    {
        int i = 1;

        Utils utilsMock = Mockito.mock(Utils.class);
        MyBranching myBranching = new MyBranching(utilsMock);

        myBranching.switchExample(i);

        //Проверка, вызывается ли функция utilFunc2(), один раз.
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1(Mockito.anyString());

    }

    /**
     * Проверка сценария, когда, в функцию подаётся на вход, число i = 2
     * должен вызваться метод utilFunc2()
     */
    @Test
    public void switchExample_Two()
    {
        int i = 2;

        Utils utilsMock = Mockito.mock(Utils.class);
        MyBranching myBranching = new MyBranching(utilsMock);

        myBranching.switchExample(i);

        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsMock, Mockito.never()).utilFunc1(Mockito.anyString());
    }

    /**
     * Проверка сценария, когда, в функцию подаётся на вход, число i = 0
     * должен вызваться метод utilFunc2() и метод utilFunc1(anyString)
     */
    @Test
    public void switchExample_Zero()
    {
        int i = 0;

        Utils utilsMock = Mockito.mock(Utils.class);
        MyBranching myBranching = new MyBranching(utilsMock);

        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        myBranching.switchExample(i);

        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1(Mockito.anyString());
    }
}
